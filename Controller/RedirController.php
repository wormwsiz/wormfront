<?php
/**
 * Created by PhpStorm.
 * User: PawełOrzechowski
 * Date: 28.05.2018
 * Time: 19:55
 */

class RedirController
{

    function redirect($url, $statusCode = 303)
    {
        header('Location: ' . $url, true, $statusCode);
        die();
    }

}