<?php
/**
 * Created by PhpStorm.
 * User: PawełOrzechowski
 * Date: 27.05.2018
 * Time: 13:56
 */

require_once("RequestsController.php");
require_once("Endpoint.php");

class UserController
{

    public function login($username, $password)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $postData = array(
            'username' => $username,
            'password' => $password
        );

        $response = $requestcontroller->createPostRequest($endpoint->login, $postData);
        if ($response["status"] == 200) {

            $_SESSION["logging"]["logged_days"]=$response["user"]["logging"][0]["logged_days"];
            $_SESSION["logging"]["unlogged_days"]=$response["user"]["logging"][0]["unlogged_days"];
            $_SESSION["idUser"] = $response["user"]["id"];
            $_SESSION["username"] = $response["user"]["username"];
            $_SESSION["worm"]["id"] = $response["user"]["wormId"];
            $_SESSION["worm"]["name"] = $response["user"]["wormName"];
            return true;
        } else {
            print_r("<p style='background-color: red'> Podano błędne dane</p>");
            return false;
        }
    }

    public function forgotPassword($email)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $postData = array(
            'email' => $email
        );

        $response = $requestcontroller->createPostRequest($endpoint->forgotPassword, $postData);

        if ($response["status"] == 200) {
            header("location:index.php");
            print_r("<p style='background-color: green'>Twoje nowe hasło znajdziesz na swojej skrzynce email.</p>");
        } else {
            print_r("<p style='background-color: red'> Podano błędne dane</p>");
        }
    }

    public function registerUser($username, $password, $email)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $postData = array(
            'username' => $username,
            'password' => $password,
            'email' => $email
        );

        $response = $requestcontroller->createPostRequest($endpoint->register, $postData);
        if ($response["status"] == 200) {
            $_SESSION["logging"]["logged_days"]=0;
            $_SESSION["logging"]["unlogged_days"]=0;
            $_SESSION["idUser"]=$response["user"]["id"];
            $_SESSION["userId"] = $response["user"]["id"];
            $_SESSION["username"] = $response["user"]["username"];
            return true;
        } else {
            return false;
        }

    }

    public function contactForm($address, $name, $email, $phone, $msg_content)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $postData = array(
            'address' => $address,
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'msg_content' => $msg_content

        );

        $response = $requestcontroller->createPostRequest($endpoint->contactForm, $postData);
        if ($response["status"] == 200) {
            return true;
        } else {
            return false;
        }
    }

    public function getRanking()
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();

        $response = $requestcontroller->createGetRequest($endpoint->getRanking);
        if ($response["status"] == 200) {
            return $response;
        } else {
            return false;
        }
    }

    public function addLifeAction($nameaction, $idworm, $eat, $sleep, $fun, $time)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $postData = array(
            'nameaction' => $nameaction,
            'idworm' => $idworm,
            'eat' => $eat,
            'sleep' => $sleep,
            'fun' => $fun,
            'time' => $time

        );

        $response = $requestcontroller->createPostRequest($endpoint->addLifeAction,$postData);

        if ($response["status"] == 200) {
            return true;
        } else {
            return false;
        }
    }

    public function getLifeActions()
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $response = $requestcontroller->createGetRequest($endpoint->getLifeActions . $_SESSION["worm"]["id"]);
        if ($response["status"] == 200) {
            return $response;
        } else {
            return false;
        }
    }

    public function breakLifeAction($idAction)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $response = $requestcontroller->createGetRequest($endpoint->breakLifeAction . $idAction);
        if ($response["status"] == 200) {
            return $response;
        } else {
            return false;
        }
    }


    public function getUser()
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $response = $requestcontroller->createGetRequest($endpoint->getUser.$_SESSION["idUser"]);
        if ($response["status"] == 200) {
            return $response;
        } else {
            return false;
        }
    }

    public function editUser($username, $email, $wormname)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $postData = array(
            'username' => $username,
            'email' => $email,
            'wormname' => $wormname

        );
        $response = $requestcontroller->createPostRequest($endpoint->editUser .  $_SESSION["idUser"], $postData);
        if ($response["status"] == 200) {
            return $response;
        } else {
            return false;
        }
    }
}