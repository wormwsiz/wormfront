<?php
/**
 * Created by PhpStorm.
 * User: PawełOrzechowski
 * Date: 26.04.2018
 * Time: 18:19
 */

class Endpoint
{

    public $baseurl= "http://145.239.93.232:8080/worm/api/";
    public $login = "http://145.239.93.232:8080/worm/api/user/login";
    public $register="http://145.239.93.232:8080/worm/api/user/register";
    public $changePassword = "http://145.239.93.232:8080/worm/api/user/paswordchange";
    public $forgotPassword = "http://145.239.93.232:8080/worm/api/user/forgotpassword";
    public $createWorm = "http://145.239.93.232:8080/worm/api/worm/create";
    public $renameWorm = "http://145.239.93.232:8080/worm/api/worm/rename";
    public $getWorm = "http://145.239.93.232:8080/worm/api/worm/get/";
    public $contactForm="http://145.239.93.232:8080/worm/api/user/contact";
    public $getRanking="http://0011.pl:8080/worm/api/rank/get";
    public $addLifeAction="http://0011.pl:8080/worm/api/lifeaction/addAction";
    public $getLifeActions="http://0011.pl:8080/worm/api/lifeaction/get/";
    public $breakLifeAction="http://0011.pl:8080/worm/api/lifeaction/break/";
    public $editUser="http://0011.pl:8080/worm/api/user/edit/";
    public $getUser="http://0011.pl:8080/worm/api/user/get/";




}