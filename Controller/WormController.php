<?php
require_once("RequestsController.php");
require_once("Endpoint.php");

class WormController
{

    public function createWorm($userId, $wormName)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $postData = array(
            'name' => $wormName,
            'userId' => $userId
        );


        $response = $requestcontroller->createPostRequest($endpoint->createWorm, $postData);

        if ($response["status"] == 200) {
            $_SESSION["worm"] = $response["worm"];
            return true;
        } else {
            return false;
        }
    }

    public function getWorm($idWorm)
    {
        $requestcontroller = new RequestsController();
        $endpoint = new Endpoint();
        $response = $requestcontroller->createGetRequest($endpoint->getWorm . $idWorm);

        if ($response["status"] == 200) {

            return $response;
        } else {
            return false;
        }
    }

}