<?php
/**
 * Created by PhpStorm.
 * User: PawełOrzechowski
 * Date: 27.05.2018
 * Time: 13:58
 */

class RequestsController
{

    public function createPostRequest($url, $postData)
    {
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        $response = curl_exec($ch);
        return json_decode($response, TRUE);
    }

    public function createGetRequest($url)
    {
        $ch = curl_init($url);

        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),

        ));

        $response = curl_exec($ch);
        return json_decode($response, TRUE);

    }

    public function createDeleteRequest($url, $token)
    {
        $ch = curl_init($url);
        if ($token) {
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_CUSTOMREQUEST => "DELETE",
                CURLOPT_HTTPHEADER => array(

                    'token:' . $token,
                    'Content-Type: application/json'
                )
            ));
        } else {
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),

            ));

        }
        $response = curl_exec($ch);
        return json_decode($response, TRUE);

    }


}