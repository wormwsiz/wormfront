        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );


window.onload = function() {

var options = {
    title: {
        text: "Funkcje życiowe"
    },
    data: [{
            type: "pie",
            startAngle: 45,
            showInLegend: "true",
            legendText: "{label}",
            indexLabel: "{label} ({y})",
            yValueFormatString:"#,##0.#"%"",
            dataPoints: [
                { label: "Życie", y: 36 },
                { label: "Engeria", y: 31 },
                { label: "Jedzenie", y: 7 },
                { label: "Szczęście", y: 7 },
                { label: "Zdrowie", y: 6 },
                { label: "Siła", y: 10 },
                { label: "Zmęczenie", y: 3 }
            ]
    }]
};
$("#chartContainer").CanvasJSChart(options);

}

window.onload = function () {

var totalVisitors = 883000;
var visitorsData = {
    "Twoje powroty na stronę": [{
        cursor: "pointer",
        explodeOnClick: false,
        innerRadius: "75%",
        legendMarkerType: "square",
        name: "Twoje powroty na stronę",
        radius: "100%",
        showInLegend: true,
        startAngle: 90,
        type: "doughnut",
        dataPoints: [
            { y: 519960, name: "Dostępny", color: "#E7823A" },
            { y: 363040, name: "Niedostępny", color: "#546BC1" }
        ]
    }],
    "New Visitors": [{
        color: "#E7823A",
        name: "Dostępny",
        type: "column",
        xValueFormatString: "MMM YYYY",
        dataPoints: [
            { x: new Date("1 Jan 2015"), y: 33000 },
            { x: new Date("1 Feb 2015"), y: 35960 },
            { x: new Date("1 Mar 2015"), y: 42160 },
            { x: new Date("1 Apr 2015"), y: 42240 },
            { x: new Date("1 May 2015"), y: 43200 },
            { x: new Date("1 Jun 2015"), y: 40600 },
            { x: new Date("1 Jul 2015"), y: 42560 },
            { x: new Date("1 Aug 2015"), y: 44280 },
            { x: new Date("1 Sep 2015"), y: 44800 },
            { x: new Date("1 Oct 2015"), y: 48720 },
            { x: new Date("1 Nov 2015"), y: 50840 },
            { x: new Date("1 Dec 2015"), y: 51600 }
        ]
    }],
    "Niedostępny": [{
        color: "#546BC1",
        name: "Niedostępny",
        type: "column",
        xValueFormatString: "MMM YYYY",
        dataPoints: [
            { x: new Date("1 Jan 2015"), y: 22000 },
            { x: new Date("1 Feb 2015"), y: 26040 },
            { x: new Date("1 Mar 2015"), y: 25840 },
            { x: new Date("1 Apr 2015"), y: 23760 },
            { x: new Date("1 May 2015"), y: 28800 },
            { x: new Date("1 Jun 2015"), y: 29400 },
            { x: new Date("1 Jul 2015"), y: 33440 },
            { x: new Date("1 Aug 2015"), y: 37720 },
            { x: new Date("1 Sep 2015"), y: 35200 },
            { x: new Date("1 Oct 2015"), y: 35280 },
            { x: new Date("1 Nov 2015"), y: 31160 },
            { x: new Date("1 Dec 2015"), y: 34400 }
        ]
    }]
};

var newVSReturningVisitorsOptions = {
    animationEnabled: true,
    theme: "light2",
    title: {
        text: "Twoje powroty na stronę"
    },
    subtitles: [{
        backgroundColor: "#2eacd1",
        fontSize: 16,
        fontColor: "white",
        padding: 5
    }],
    legend: {
        fontFamily: "calibri",
        fontSize: 14,
        itemTextFormatter: function (e) {
            return e.dataPoint.name + ": " + Math.round(e.dataPoint.y / totalVisitors * 100) + "%";  
        }
    },
    data: []
};

var visitorsDrilldownedChartOptions = {
    animationEnabled: true,
    theme: "light2",
    axisX: {
        labelFontColor: "#717171",
        lineColor: "#a2a2a2",
        tickColor: "#a2a2a2"
    },
    axisY: {
        gridThickness: 0,
        includeZero: false,
        labelFontColor: "#717171",
        lineColor: "#a2a2a2",
        tickColor: "#a2a2a2",
        lineThickness: 1
    },
    data: []
};

newVSReturningVisitorsOptions.data = visitorsData["Twoje powroty na stronę"];
$("#chartContainer").CanvasJSChart(newVSReturningVisitorsOptions);

function visitorsChartDrilldownHandler(e) {
    e.chart.options = visitorsDrilldownedChartOptions;
    e.chart.options.data = visitorsData[e.dataPoint.name];
    e.chart.options.title = { text: e.dataPoint.name }
    e.chart.render();
    $("#backButton").toggleClass("invisible");
}

$("#backButton").click(function() { 
    $(this).toggleClass("invisible");
    newVSReturningVisitorsOptions.data = visitorsData["Twoje powroty na stronę"];
    $("#chartContainer").CanvasJSChart(newVSReturningVisitorsOptions);
});



var chart = new CanvasJS.Chart("chartContainer1", {
    animationEnabled: true,
    title:{
        text: "Parametry robaczka"
    },
    axisX: {
        valueFormatString: "DDD"
    },
    axisY: {
        prefix: "$"
    },
    toolTip: {
        shared: true
    },
    legend:{
        cursor: "pointer",
        itemclick: toggleDataSeries
    },
    data: [{
        type: "stackedBar",
        name: "Jedzenie",
        showInLegend: "true",
        xValueFormatString: "DD, MMM",
        yValueFormatString: "$#,##0",
        dataPoints: [
            { x: new Date(2017, 0, 30), y: 56 },
            { x: new Date(2017, 0, 31), y: 45 },
            { x: new Date(2017, 1, 1), y: 71 },
            { x: new Date(2017, 1, 2), y: 41 },
            { x: new Date(2017, 1, 3), y: 60 },
            { x: new Date(2017, 1, 4), y: 75 },
            { x: new Date(2017, 1, 5), y: 98 }
        ]
    },
    {
        type: "stackedBar",
        name: "Sczęście",
        showInLegend: "true",
        xValueFormatString: "DD, MMM",
        yValueFormatString: "$#,##0",
        dataPoints: [
            { x: new Date(2017, 0, 30), y: 86 },
            { x: new Date(2017, 0, 31), y: 95 },
            { x: new Date(2017, 1, 1), y: 71 },
            { x: new Date(2017, 1, 2), y: 58 },
            { x: new Date(2017, 1, 3), y: 60 },
            { x: new Date(2017, 1, 4), y: 65 },
            { x: new Date(2017, 1, 5), y: 89 }
        ]
    },
    {
        type: "stackedBar",
        name: "Napoje",
        showInLegend: "true",
        xValueFormatString: "DD, MMM",
        yValueFormatString: "$#,##0",
        dataPoints: [
            { x: new Date(2017, 0, 30), y: 48 },
            { x: new Date(2017, 0, 31), y: 45 },
            { x: new Date(2017, 1, 1), y: 41 },
            { x: new Date(2017, 1, 2), y: 55 },
            { x: new Date(2017, 1, 3), y: 80 },
            { x: new Date(2017, 1, 4), y: 85 },
            { x: new Date(2017, 1, 5), y: 83 }
        ]
    },
    {
        type: "stackedBar",
        name: "Smutek",
        showInLegend: "true",
        xValueFormatString: "DD, MMM",
        yValueFormatString: "$#,##0",
        dataPoints: [
            { x: new Date(2017, 0, 30), y: 61 },
            { x: new Date(2017, 0, 31), y: 55 },
            { x: new Date(2017, 1, 1), y: 61 },
            { x: new Date(2017, 1, 2), y: 75 },
            { x: new Date(2017, 1, 3), y: 80 },
            { x: new Date(2017, 1, 4), y: 85 },
            { x: new Date(2017, 1, 5), y: 105 }
        ]
    },
    {
        type: "stackedBar",
        name: "Sen",
        showInLegend: "true",
        xValueFormatString: "DD, MMM",
        yValueFormatString: "$#,##0",
        dataPoints: [
            { x: new Date(2017, 0, 30), y: 52 },
            { x: new Date(2017, 0, 31), y: 55 },
            { x: new Date(2017, 1, 1), y: 20 },
            { x: new Date(2017, 1, 2), y: 35 },
            { x: new Date(2017, 1, 3), y: 30 },
            { x: new Date(2017, 1, 4), y: 45 },
            { x: new Date(2017, 1, 5), y: 25 }
        ]
    }]
});
chart.render();

function toggleDataSeries(e) {
    if(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}

}



