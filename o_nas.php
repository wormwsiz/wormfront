<?php
require_once("Controller/UserController.php");
require_once("Controller/RedirController.php");
$userController = new UserController();
$redirController = new RedirController();
session_start();
$flag = 0;

if (isset($_POST["sendForm"])) {

    if ($userController->contactForm($_POST["email"], $_POST["name"], $_POST["email"], $_POST["phone"], $_POST["comment"])) {
        $flag = 1;
    } else
        $flag = 2;


}
?>

<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Worm game!</title>
    <meta name="description" content="Worm game! to wciąga!">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="stylesheet" href="assets/css/game.css">
    <link rel="stylesheet" href="assets/css/sidepanel.css">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="assets/scss/style.css">
    <link rel="stylesheet" href="assets/css/preloader.css">



    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body class="open">
<!-- Left Panel -->

<!-- Left Panel -->
<div id="preloaderek">
    <div class="bg_white"></div>
    <div class="bg_loader">
        <div class="loader"></div>
    </div>
</div>

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="">Worm Game<img src="" alt=""></a>
            <a class="navbar-brand hidden" href="">W<img src="" alt=""></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="game.php"> <i class="menu-icon fa fa-dashboard"></i>Strona główna</a>
                </li>
                <h3 class="menu-title">Panel gry</h3><!-- /.menu-title -->
                <li class="">
                    <a href="uzytkownicy.php"> <i class="menu-icon fa fa-table"></i>Użytkownicy</a>
                </li>
                <li class="">
                    <a href="wykresy.php"> <i class="menu-icon fa fa-bar-chart"></i>Wykresy</a>
                </li>
                <h3 class="menu-title">Moje konto</h3><!-- /.menu-title -->
                <li class="">
                    <a href="ustawienia.php"> <i class="menu-icon fa fa-th"></i>Ustawienia</a>
                </li>
                <li class="">
                    <a href="o_nas.php"> <i class="menu-icon fa fa-bars"></i>Napisz do nas</a>
                </li>
                <li class="">
                    <a href="logout.php"> <i class="menu-icon fa fa-tasks"></i>Wyloguj</a>
                </li>
                <li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Right Panel -->

<script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/plugins.js"></script>
<div id="right-panel" class="right-panel">

    <!-- Header-->
    <header id="header" class="header">

        <div class="header-menu">

            <div class="col-sm-7">
                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                <div class="header-left">
                    <button class="search-trigger"><i class="fa fa-search"></i></button>
                    <div class="form-inline">
                        <form class="search-form">
                            <input class="form-control mr-sm-2" type="text" placeholder="Szukaj ..."
                                   aria-label="Search">
                            <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                        </form>
                    </div>

                    <div class="dropdown for-notification">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="notification"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="count bg-danger">3</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="notification">
                            <p class="red">Masz 3 powiadomienia</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <i class="fa fa-check"></i>
                                <p>Przeciążenie serwera #1</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <i class="fa fa-info"></i>
                                <p>Przeciążenie serwera #2</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <i class="fa fa-warning"></i>
                                <p>Przeciążenie serwera #3</p>
                            </a>
                        </div>
                    </div>

                    <div class="dropdown for-message">
                        <button class="btn btn-secondary dropdown-toggle" type="button"
                                id="message"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-email"></i>
                            <span class="count bg-primary">9</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="message">
                            <p class="red">Masz 4 wiadomości</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jonathan Smith</span>
                                    <span class="time float-right">Teraz</span>
                                        <p>Cześć, przykładowa wiadomość nr 1!</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jack Sanders</span>
                                    <span class="time float-right">5 temu</span>
                                        <p>Cześć, przykładowa wiadomość nr 2</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Cheryl Wheeler</span>
                                    <span class="time float-right">10 temu</span>
                                        <p>Cześć, przykładowa wiadomość nr 3!</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-3" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Rachel Santos</span>
                                    <span class="time float-right">15 temu</span>
                                        <p>Cześć, przykładowa wiadomość nr 4!</p>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-5">
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                    </a>

                    <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="index.php"><i class="fa fa- user"></i>Mój profil</a>

                        <a class="nav-link" href="index.php"><i class="fa fa- user"></i>Powiadomienia <span
                                    class="count">13</span></a>

                        <a class="nav-link" href="ustawienia.php"><i class="fa fa -cog"></i>Ustawienia</a>

                        <a class="nav-link" href="logout.php"><i class="fa fa-power -off"></i>Wyloguj</a>
                    </div>
                </div>

                <div class="language-select dropdown" id="language-select">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="language" aria-haspopup="true"
                       aria-expanded="true">
                        <i class="flag-icon flag-icon-pl"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="language">
                        <div class="dropdown-item">
                            <span class="flag-icon flag-icon-fr"></span>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-es"></i>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-us"></i>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-pl"></i>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </header><!-- /header -->
    <!-- Header-->
    <!-- Header-->
    <div class="content mt-3" style="margin-top: 0.5rem!important">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Informacje kontaktowe</strong>
                            <small><br>*Napisz do nas!</small>
                        </div>
                        <div class="card-body" style="min-height: 700px">
                            <div id="position-container">
                            <div class="row" style="text-align: center;">
                                <hr>
                                <div class="col-sm-12" id="parent">
                                    <div class="col-sm-6">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2561.995992102286!2d21.97951291524579!3d50.04890637942195!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473cfb6d2f4ddf8f%3A0x958858da08f8753b!2sWy%C5%BCsza+Szko%C5%82a+Informatyki+i+Zarz%C4%85dzania!5e0!3m2!1spl!2spl!4v1527491401517"
                                                width="100%" height="320px" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>

                                    <div class="col-sm-6">
                                        <?php
                                        if ($flag == 1)
                                            print_r("<div><p style='background-color: green;color: white;'> Wiadomość została wysłana poprawnie!</p>  </div>");

                                        if ($flag == 2)
                                            print_r("<div><p style='background-color: red;color: white;'>  Wiadomość została wysłana poprawnie!</p>  </div>");

                                        ?>

                                        <form class="contact-form" id="contactform" method="post">

                                            <div class="form-group">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Imię i nazwisko"
                                                       required="" autofocus="">
                                            </div>


                                            <div class="form-group form_left">
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                                       required="">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" class="form-control" id="phone" name="phone"
                                                       onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10"
                                                       placeholder="Telefon" required="">
                                            </div>
                                            <div class="form-group">
                            <textarea class="form-control textarea-contact" rows="5" id="comment" name="comment"
                                      placeholder="Wprowadź wiadomość" required=""></textarea>
                                                <br>
                                                <button type="submit" class="btn btn-default btn-send" name="sendForm"><span
                                                            class="glyphicon glyphicon-send"></span> Wyślij
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="container second-portion">


                                <div class="row">
                                    <!-- Boxes de Acoes -->
                                    <div class="col-xs-12 col-sm-6 col-lg-4">
                                        <div class="box">
                                            <div class="icon">
                                                <div class="image"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                                                <div class="info">
                                                    <h3 class="title">MAIL & WEBSITE</h3>
                                                    <p>
                                                        <i class="fa fa-envelope" aria-hidden="true"></i> &nbsp gondhiyahardik6610@gmail.com
                                                        <br>
                                                        <br>
                                                        <i class="fa fa-globe" aria-hidden="true"></i> &nbsp www.hardikgondhiya.com
                                                    </p>

                                                </div>
                                            </div>
                                            <div class="space"></div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-lg-4">
                                        <div class="box">
                                            <div class="icon">
                                                <div class="image"><i class="fa fa-mobile" aria-hidden="true"></i></div>
                                                <div class="info">
                                                    <h3 class="title">CONTACT</h3>
                                                    <p>
                                                        <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp (+91)-9624XXXXX
                                                        <br>
                                                        <br>
                                                        <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp (+91)-7567065254
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="space"></div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-lg-4">
                                        <div class="box">
                                            <div class="icon">
                                                <div class="image"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                                <div class="info">
                                                    <h3 class="title">ADDRESS</h3>
                                                    <p>
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp 15/3 Junction Plot
                                                        "Shree Krishna Krupa", Rajkot - 360001.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="space"></div>
                                        </div>
                                    </div>
                                    <!-- /Boxes de Acoes -->

                                    <!--My Portfolio  dont Copy this -->

                                </div>
                            </div>
                        </div>
                        <div class="card-footer" style="text-align: center">
                            <strong class="card-title">Projekt wykonali: </strong><br>
                            <small style="margin-right: 30px">Wojciech Żeleźnik 58057;</small>
                            <small style="margin-right: 30px">Paweł Orzechowski 58045;</small>
                            <small style="margin-right: 30px">Daniel Mroczka 58049;</small>
                            <small style="margin-right: 30px">Krzysztof Skrobacz 58069;</small>
                            <small style="margin-right: 30px">Piotr Kocot 49985;</small>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


    <script type="text/javascript">
        $(function(){
            $('#contactform').submit(function() {
                $('#preloaderek').css("visibility","visible")
            });
        });
    </script>

</div><!-- /#right-panel -->
<script src="assets/js/main.js"></script>
<script src="assets/js/charts.js"></script>
<script src="assets/js/lib/data-table/datatables.min.js"></script>
<script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="assets/js/lib/data-table/jszip.min.js"></script>
<script src="assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="assets/js/lib/data-table/datatables-init.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script type="text/javascript" src="assets/js/charts.js"></script>
<!-- <link rel="stylesheet" href="assets/js/charts.js"> WTF nie ładuje :/ -->


<script type="text/javascript">
    $(document).ready(function () {
        $('#bootstrap-data-table-export').DataTable();
    });
</script>



</body>
</html>
