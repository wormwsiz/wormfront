<?php
require_once("Controller/UserController.php");
require_once("Controller/WormController.php");
require_once("Controller/RedirController.php");

$userController = new UserController();
$wormController = new WormController();
$redirController = new RedirController();
session_start();


if (isset($_POST["username"]) && isset($_POST["password"])) {

    if($userController->login($_POST["username"], $_POST["password"])){

        $redirController->redirect("game.php",301);
    }


}

if (isset($_POST["register-submit"])) {

    if ($_POST["register_password"] != $_POST["register_confirm-password"]) {
        print_r("<p style='background-color: red'> Podano błędne dane</p>");
        return;
    }
    if ($userController->registerUser($_POST["register_username"], $_POST["register_password"], $_POST["register_email"]) && $wormController->createWorm($_SESSION["userId"], $_POST["register_wormname"])) {

        $_SESSION["logged"] = true;
        $redirController->redirect("game.php",301);


    } else print_r("<p style='background-color: red'> Wystąpił błąd, spróbuj ponownie!</p>");


}
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>Logowanie / rejestracja</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="assets/css/index.css">
    <!-- <link rel="stylesheet" href="js/worm.js"> -->
</head>
<body class="open">
<div class="container">


    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="logo-container">
                <img src="images/LogoX.png">
            </div>
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="#" class="active" id="login-form-link">Zaloguj się</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" id="register-form-link">Zarejestruj!</a>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="login-form" method="post" role="form" name="loginForm" style="display: block;">
                                <div class="form-group">
                                    <input type="text" name="username" id="username" tabindex="1" class="form-control"
                                           placeholder="Nazwa użytkownika" value="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="password" tabindex="2"
                                           class="form-control" placeholder="Hasło">
                                </div>
                                <div class="form-group text-center">
                                    <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                    <label for="remember"> Zapamiętaj mnie</label>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4"
                                                   class="form-control btn btn-login" value="Zaloguj">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="text-center">
                                                <a href="reset_password.php" tabindex="5" class="forgot-password">Zapomniałeś
                                                    hasła?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form name="register-form" id="register-form" method="post" value="register-form"
                                  style="display: none;">

                                <div class="form-group">
                                    <input type="text" name="register_wormname" id="register_wormname" tabindex="1"
                                           class="form-control"
                                           placeholder="Imię Twojej postaci" value="">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="register_username" id="register_username" tabindex="1"
                                           class="form-control"
                                           placeholder="Nazwa użytkownika" value="">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="register_email" id="register_email" tabindex="1"
                                           class="form-control"
                                           placeholder="Adres e-mail" value="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="register_password" id="register_password" tabindex="2"
                                           class="form-control" placeholder="Hasło">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="register_confirm-password"
                                           id="register_confirm-password" tabindex="2"
                                           class="form-control" placeholder="Potwierdź hasło">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="register-submit" id="register-submit"
                                                   tabindex="4" class="form-control btn btn-register"
                                                   value="Zarejestruj">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {

        $('#login-form-link').click(function (e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#register-form-link').click(function (e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

    });

</script>
</body>
</html>
