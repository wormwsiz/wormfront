<?php
require_once("Controller/UserController.php");
require_once("Controller/WormController.php");
require_once("Controller/RedirController.php");

$userController = new UserController();
$wormController = new WormController();
$redirController = new RedirController();
session_start();

$user = $userController->getUser();

if (isset($_POST["username"]) && isset($_POST["password"])) {
    $userController->login($_POST["username"], $_POST["password"]);
}
$_SESSION["wormData"] = $wormController->getWorm($_SESSION["worm"]["id"]);

$lifeActions = $userController->getLifeActions();
if (!isset($lifeActions["actions"][0][0])) {
    $lifeActions = null;
}

if (isset($_GET["break"])) {
    $userController->breakLifeAction($_GET["id"]);
    $redirController->redirect("game.php");

}
if (isset($_GET["action"])) {

    $userController->addLifeAction($_GET["action"], $_SESSION["worm"]["id"], $_GET["eat"], $_GET["sleep"], $_GET["fun"], $_GET["time"]);
    $redirController->redirect("game.php");
}

?>
<!doctype html>

<head xmlns="http://www.w3.org/1999/html">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Worm game!</title>
    <meta name="description" content="Worm game! to wciąga!">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="stylesheet" href="assets/css/game.css">
    <link rel="stylesheet" href="assets/css/progressbar.css">
    <link rel="stylesheet" href="assets/css/sidepanel.css">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="assets/scss/style.css">
    <link rel="stylesheet" href="assets/scss/progress-bar.scss">
    <link rel="stylesheet" href="assets/scss/happinessmeter.scss">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>


    <!---------------------------------JS----------------------------------------->
    <!--------------------------Happiness meter----------------------------------->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.13/angular.min.js"></script>
    <script src="assets/js/gauge.min.js"></script>
    <script src="assets/js/happinessgauge.js"></script>


</head>

<body class="open">
<!-- Left Panel -->

<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="">Worm Game<img src="" alt=""></a>
            <a class="navbar-brand hidden" href="">W<img src="" alt=""></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="game.php"> <i class="menu-icon fa fa-dashboard"></i>Strona główna</a>
                </li>
                <h3 class="menu-title">Panel gry</h3><!-- /.menu-title -->
                <li class="">
                    <a href="uzytkownicy.php"> <i class="menu-icon fa fa-table"></i>Użytkownicy</a>
                </li>
                <li class="">
                    <a href="wykresy.php"> <i class="menu-icon fa fa-bar-chart"></i>Wykresy</a>
                </li>
                <h3 class="menu-title">Moje konto</h3><!-- /.menu-title -->
                <li class="">
                    <a href="ustawienia.php"> <i class="menu-icon fa fa-th"></i>Ustawienia</a>
                </li>
                <li class="">
                    <a href="o_nas.php"> <i class="menu-icon fa fa-bars"></i>Napisz do nas</a>
                </li>
                <li class="">
                    <a href="logout.php"> <i class="menu-icon fa fa-tasks"></i>Wyloguj</a>
                </li>
                <li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Right Panel -->

<script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/plugins.js"></script>
<div id="right-panel" class="right-panel">

    <!-- Header-->
    <header id="header" class="header">

        <div class="header-menu">

            <div class="col-sm-7">
                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                <div class="header-left">
                    <button class="search-trigger"><i class="fa fa-search"></i></button>
                    <div class="form-inline">
                        <form class="search-form">
                            <input class="form-control mr-sm-2" type="text" placeholder="Szukaj ..."
                                   aria-label="Search">
                            <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                        </form>
                    </div>

                    <div class="dropdown for-notification">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="notification"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="count bg-danger">3</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="notification">
                            <p class="red">Masz 2 powiadomienia</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <i class="fa fa-check"></i>
                                <p>Przeciążenie serwera #1</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <i class="fa fa-info"></i>
                                <p>Przeciążenie serwera #2</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <i class="fa fa-warning"></i>
                                <p>Przeciążenie serwera #3</p>
                            </a>
                        </div>
                    </div>

                    <div class="dropdown for-message">
                        <button class="btn btn-secondary dropdown-toggle" type="button"
                                id="message"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-email"></i>
                            <span class="count bg-primary">9</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="message">
                            <p class="red">Masz 4 wiadomości</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jonathan Smith</span>
                                    <span class="time float-right">Teraz</span>
                                        <p>Cześć, przykładowa wiadomość nr 1!</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jack Sanders</span>
                                    <span class="time float-right">5 temu</span>
                                        <p>Cześć, przykładowa wiadomość nr 2</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Cheryl Wheeler</span>
                                    <span class="time float-right">10 temu</span>
                                        <p>Cześć, przykładowa wiadomość nr 3!</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-3" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Rachel Santos</span>
                                    <span class="time float-right">15 temu</span>
                                        <p>Cześć, przykładowa wiadomość nr 4!</p>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-5">
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                        <h7><?php echo $_SESSION["username"] ?></h7>
                    </a>


                    <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="html-php%20już%20zmienione/index.php"><i class="fa fa- user"></i>Mój
                            profil</a>

                        <a class="nav-link" href="html-php%20już%20zmienione/index.html"><i class="fa fa- user"></i>Powiadomienia
                            <span class="count">13</span></a>

                        <a class="nav-link" href="ustawienia.php"><i class="fa fa -cog"></i>Ustawienia</a>

                        <a class="nav-link" href="logout.php"><i class="fa fa-power -off"></i>Wyloguj</a>
                    </div>
                </div>

                <div class="language-select dropdown" id="language-select">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="language" aria-haspopup="true"
                       aria-expanded="true">
                        <i class="flag-icon flag-icon-pl"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="language">
                        <div class="dropdown-item">
                            <span class="flag-icon flag-icon-fr"></span>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-es"></i>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-us"></i>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-pl"></i>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </header><!-- /header -->
    <!-- Header-->
    <div class="content mt-3" style="margin-top: 0.5rem!important">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">WORM GAME</strong>
                            <small><br>*Opiekuj się robakiem!</small>
                        </div>
                        <div class="card-body" style="min-height: 700px">
                            <div id="position-container">
                                <div class="game-container">
                                    <ul class="action-box">
                                        <div id="menu-tekst">Menu</div>
                                        <li id="menu-button" class="nav-menu-button" style="background-image: url('images/icons/Jedzenie.png');">
                                            <table class="action-table">
                                                <tr>
                                                    <td><a href="game.php?action=Kanapka&eat=15&sleep=-5&fun=0&time=5"><img src="images/icons/Kanapka.png" /></a></td>
                                                    <td><a href="game.php?action=Hamburger&eat=30&sleep=-15&fun=5&time=10"><img src="images/icons/Hamburger.png" /></a></td>
                                                    <td><a href="game.php?action=owoc&eat=10&sleep=0&fun=0&time=5"><img src="images/icons/Owoc.png" /></a></td>
                                                </tr>
                                                <tr>
                                                    <th>Kanapka (5m)</th>
                                                    <th>FastFood (10m)</th>
                                                    <th>Owoc (5m)</th>
                                                </tr>
                                                <tr>
                                                    <td>Energia: <h id="debuff">-5</h></td>
                                                    <td>Energia: <h id="debuff">-15</h></td>
                                                    <td>Energia: <h>0</h></td>
                                                </tr>
                                                <tr>
                                                    <td>Sytość: <h id="buff">+15</h></td>
                                                    <td>Sytość: <h id="buff">+30</h></td>
                                                    <td>Sytość: <h id="buff">+10</h></td>
                                                </tr>
                                                <tr>
                                                    <td>Humor: <h>0</h></td>
                                                    <td>Humor: <h id="buff">5</h></td>
                                                    <td>Humor: <h>0</h></td>
                                                </tr>
                                            </table>
                                        </li>
                                        <li id="menu-button" class="nav-menu-button" style="background-image: url('images/icons/Picie.png');">
                                            <table class="action-table">
                                                <tr>
                                                    <td><a href="game.php?action=Woda&eat=5&sleep=0&fun=0&time=2"><img src="images/icons/Woda.png" /></a></td>
                                                    <td><a href="game.php?action=Kawa&eat=5&sleep=30&fun=0&time=10"><img src="images/icons/Kawa.png" /></a></td>
                                                    <td><a href="game.php?action=Piwo&eat=-10&sleep=-10&fun=15&time=10"><img src="images/icons/Piwo.png" /></a></td>
                                                </tr>
                                                <tr>
                                                    <th>Woda (2m)</th>
                                                    <th>Kawa (10m)</th>
                                                    <th>Piwo (10m)</th>
                                                </tr>
                                                <tr>
                                                    <td>Energia: <h>0</h></td>
                                                    <td>Energia: <h id="buff">+30</h></td>
                                                    <td>Energia: <h id="debuff">-10</h></td>
                                                </tr>
                                                <tr>
                                                    <td>Sytość: <h id="buff">+5</h></td>
                                                    <td>Sytość: <h id="buff">+5</h></td>
                                                    <td>Sytość: <h id="debuff">-10</h></td>
                                                </tr>
                                                <tr>
                                                    <td>Humor: <h>0</h></td>
                                                    <td>Humor: <h>0</h></td>
                                                    <td>Humor: <h id="buff">+15</h></td>
                                                </tr>
                                            </table>
                                        </li>
                                        <li id="menu-button" class="nav-menu-button" style="background-image: url('images/icons/Zabawa.png');">
                                            <table class="action-table">
                                                <tr>
                                                    <td><a href="game.php?action=Pilka&eat=-20&sleep=-30&fun=50&time=30"><img src="images/icons/Pilka.png" /></a></td>
                                                    <td><a href="game.php?action=Srajfon&eat=-10&sleep=-20&fun=30&time=30"><img src="images/icons/Srajfon.png" /></a></td>
                                                    <td><a href="game.php?action=Book&eat=-5&sleep=-25&fun=25&time=30"><img src="images/icons/Book.png" /></a></td>
                                                </tr>
                                                <tr>
                                                    <th>Sport (30m)</th>
                                                    <th>E-gra (30m)</th>
                                                    <th>Książka (30m)</th>
                                                </tr>
                                                <tr>
                                                    <td>Energia: <h id="debuff">-30</h></td>
                                                    <td>Energia: <h id="debuff">-20</h></td>
                                                    <td>Energia: <h id="debuff">-25</h></td>
                                                </tr>
                                                <tr>
                                                    <td>Sytość: <h id="debuff">-20</h></td>
                                                    <td>Sytość: <h id="debuff">-10</h></td>
                                                    <td>Sytość: <h id="debuff">-5</h></td>
                                                </tr>
                                                <tr>
                                                    <td>Humor: <h id="buff">50</h></td>
                                                    <td>Humor: <h id="buff">30</h></td>
                                                    <td>Humor: <h id="buff">30</h></td>
                                                </tr>
                                            </table>
                                        </li>
                                        <li id="menu-button" class="nav-menu-button" style="background-image: url('images/icons/Zycie.png');">
                                            <table class="action-table">
                                                <tr>
                                                    <td><a href="game.php?action=Sen&eat=0&sleep=100&fun=60&time=360"><img src="images/icons/Sen.png" /></a></td>
                                                    <td><a href="game.php?action=Prysznic&eat=0&sleep=-10&fun=30&time=15"><img src="images/icons/Prysznic.png" /></a></td>
                                                    <td><a><img src="images/icons/Reanimacja%20.png" /></a></td>
                                                </tr>
                                                <tr>
                                                    <th>Sen (6h)</th>
                                                    <th>Prysznic (15m)</th>
                                                    <th>Reanimuj (1d)</th>
                                                </tr>
                                                <tr>
                                                    <td>Energia: <h id="buff">+100</h></td>
                                                    <td>Energia: <h id="debuff">-10</h></td>
                                                    <td>Energia: <h id="buff">+30</h></td>
                                                </tr>
                                                <tr>
                                                    <td>Sytość: <h>0</h></td>
                                                    <td>Sytość: <h id="debuff">-10</h></td></td>
                                                    <td>Sytość: <h id="buff">+30</h></td>
                                                </tr>
                                                <tr>
                                                    <td>Humor: <h id="buff">+60</h></td>
                                                    <td>Humor: <h id="buff">+30</h></td>
                                                    <td>Humor: <h id="buff">+30</h></td>
                                                </tr>
                                            </table>
                                        </li>
                                    </ul>
                                    <div class="game-box">
                                        <ul>
                                            <li style="margin-top: 50px;">
                                                <h class="title">(<?php echo $user["worm"]["name"]; ?> )</h>
                                            </li>
                                            <li>
                                                <!---------------------------------Gif humoru worma---------------------------->
                                                <?php
                                                $happiness = intval($_SESSION["wormData"]["life"]["calculated_happy_level"] * 3.33);
                                                    if ($happiness <= 0)
                                                    {
                                                        echo '<img src="images/worm1.png">';
                                                        echo 'rip';
                                                    }
                                                    if ($happiness  > 0 && $happiness < 33)
                                                    {
                                                        echo '<img style="width: 300px" src="images/worm_gif/sad.gif">';

                                                    }
                                                    elseif ($happiness >= 33  && $happiness < 66)
                                                    {
                                                        echo '<img style="width: 300px" src="images/worm_gif/normal.gif">';

                                                    }
                                                    else
                                                    {
                                                        echo '<img style="width: 300px" src="images/worm_gif/happy.gif">';

                                                    }
                                                ?>
                                                <!---------------------------------------------------------------------------->
                                            </li>
                                        </ul>
                                    </div>
                                    <!-----------------------Progress Bar--------------------------->
                                    <div class="stat-levels">
                                        <h id="stats-tekst">Energia: <?php echo $_SESSION["wormData"]["life"]["energy"] ?>%</h>
                                        <div class="stat-1 stat-bar">
                                            <span class="stat-bar-rating" role="stat-bar"
                                                  style="width: <?php echo $_SESSION["wormData"]["life"]["energy"] ?>%;"><?php echo $_SESSION["wormData"]["life"]["energy"] ?>
                                                %</span>
                                        </div>
                                        <h id="stats-tekst">Sytość: <?php echo $_SESSION["wormData"]["life"]["hunger"] ?>%</h>
                                        <div class="stat-2 stat-bar">
                                            <span class="stat-bar-rating" role="stat-bar"
                                                  style="width: <?php echo $_SESSION["wormData"]["life"]["hunger"] ?>%;"><?php echo $_SESSION["wormData"]["life"]["hunger"] ?>
                                                %</span>
                                        </div>
                                        <h id="stats-tekst">Humor: <?php echo $_SESSION["wormData"]["life"]["fun"] ?>%</h>
                                        <div class="stat-3 stat-bar">
                                            <span class="stat-bar-rating" role="stat-bar"
                                                  style="width: <?php echo $_SESSION["wormData"]["life"]["fun"] ?>%;"><?php echo $_SESSION["wormData"]["life"]["fun"] ?>
                                                %</span>
                                        </div>
                                        <center><h id="stats-tekst">Poziom Szczęścia:</h></center>
                                        <!----------------------------Wskaźnik--------------------------------------->
                                        <div id="gauge-container">
                                            <body ng-app="gauge">
                                            <div ng-controller="test">

                                                <?php
                                                $happy = intval($_SESSION["wormData"]["life"]["calculated_happy_level"] * 3.33);

                                                ?>
                                                <canvas gaugejs options="gaugeOptions"
                                                        value="<?php echo $happy; ?>"
                                                        max-value="100" animation-time="animationTime"></canvas>
                                                <br/>
                                                <span style="color:  white;font-weight:  bold;    padding-left: 88px;"><?php echo $happy; ?>
                                                    %</span>
                                            </div>
                                            </body>
                                        </div>
                                        <!---------------------------------------------------------------------------------->
                                    </div>
                                </div>
                                    <?php if ($lifeActions != null) {
                                        echo '<div id="performed-actions-container">';
                                        foreach ($lifeActions["actions"] as $action) {
                                            foreach ($action as $a) {
                                                echo "<div id=\"action-in-progress\" style=\"background-image: url('images/icons/".$a["nameAction"].".png')\">";
                                                echo '<a href="game.php?break=true&id='.$a["id"].'"><img src="images/icons/close.png"></a>';
                                                echo '<div id="action-in-progress-time">';
                                                echo '<h>';
                                                echo $a["minutesLeft"] . " min";
                                                echo '</h>';
                                                echo '</div>';
                                                echo '</div>';
                                            }
                                        }
                                        echo '</div>';
                                    } ?>
                                    <!---------------------------------------------------------------------------->
                                </div>
                        </div>
                        <div class="card-footer" style="text-align: center">
                            <strong class="card-title">Projekt wykonali: </strong><br>
                            <small style="margin-right: 30px">Wojciech Żeleźnik 58057;</small>
                            <small style="margin-right: 30px">Paweł Orzechowski 58045;</small>
                            <small style="margin-right: 30px">Daniel Mroczka 58049;</small>
                            <small style="margin-right: 30px">Krzysztof Skrobacz 58069;</small>
                            <small style="margin-right: 30px">Piotr Kocot 49985;</small>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
</div>

<!-- /#right-panel -->
<script src="assets/js/main.js"></script>
<script src="assets/js/charts.js"></script>
<script src="assets/js/lib/data-table/datatables.min.js"></script>
<script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="assets/js/lib/data-table/jszip.min.js"></script>
<script src="assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="assets/js/lib/data-table/datatables-init.js"></script>

<script type="text/javascript" src="assets/js/charts.js"></script>
<!-- <link rel="stylesheet" href="assets/js/charts.js"> WTF nie ładuje :/ -->


<script type="text/javascript">
    $(document).ready(function () {
        $('#bootstrap-data-table-export').DataTable();
    });
</script>

</body>
</html>
