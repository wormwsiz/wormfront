<?php

require_once("Controller/UserController.php");

$userController = new UserController();
session_start();


if (isset($_POST["email"])) {

    $userController->forgotPassword($_POST["email"]);


}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Zresetuj hasło</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="assets/css/index.css">
</head>
<body>


<div id="preloaderek">
    <div class="bg_white"></div>
    <div class="bg_loader">
        <div class="loader"></div>
    </div>
</div>


<div class="container">
    <div id="passwordreset" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Resetuj hasło</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink"
                                                                                           href="index.php">Zaloguj
                        się</a></div>
            </div>
            <div class="panel-body">
                <form id="signupform" class="form-horizontal" role="form" method="post" name="signupform">
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">E-mail</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email"
                                   placeholder="Wprowadź swój adres e-mail">
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="submit" class="btn btn-warning"
                            ">Wyślij</button>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                Wyślemy Ci e-mail z linkiem do resetowania hasła.
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('#signupform').submit(function () {
            $('#preloaderek').css("visibility", "visible")
        });
    });
</script>


</body>
</html>
